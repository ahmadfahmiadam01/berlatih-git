<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di PKS Digital School!"
*/

// Code function di sini
function greetings($name){
    echo "Halo " . $name . " , Selamat Datang di PKS Digital School!<br>"; 
}

// Hapus komentar untuk menjalankan code!
greetings("Bagas");
greetings("Wahyu");
greetings("Abdul");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping 
(for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 

function reverse($kata1){
    $panjangkata = strlen($kata1);
    $wadah = "";
    for($a=($panjangkata - 1); $a>=0; $a--){
        $wadah .= $kata1[$a];
    }
    return $wadah;
}

function reverseString($kata2){
    $string1 = reverse ($kata2);
    echo $string1 ."<br>";
}

// Hapus komentar di bawah ini untuk jalankan Code
reverseString("abduh");
reverseString("Digital School");
reverseString("We Are PKS Digital School Developers");
echo "<br>";


echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari 
jawaban no.2!

*/


// Code function di sini
function balik($word1){
    $longword = strlen($word1);
    $tampung = "";
    for($b=($longword - 1); $b>=0; $b--){
        $tampung .= $word1[$b];
    }
    return $tampung;
}

function palindrome($word2){
    $string2 = balik ($word2);
    if($string2 == $word2){
        echo $string2 ." : TRUE <br>";
    } else {
        echo $string2 ." : FALSE <br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
palindrome("civic"); // true
palindrome("nababan"); // true
palindrome("jambaban"); // false
palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn 
String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika 
parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($angka){
    $output = "";
    if($angka>=98 && $angka<100){
        $output .= "Sangat Baik";
    }else if($angka >=76 && $angka<98){
        $output .= "Baik";
    }else if($angka >=67 && $angka<76){
        $output .= "Cukup";
    }else if($angka >=43 && $angka<67){
        $output .= "Kurang";
    }
    return $output ."<br>";
}


// Hapus komentar di bawah ini untuk jalankan code
echo "Nilai 98 : <br>";
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";

echo "Nilai 76 : <br>";
echo tentukan_nilai(76); //Baik
echo "<br>";

echo "Nilai 67 : <br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";

echo "Nilai 43 : <br>";
echo tentukan_nilai(43); //Kurang
echo "<br>";

?>

</body>

</html>